function doGet() {
  var output = HtmlService.createHtmlOutputFromFile('forms');
  output.addMetaTag('viewport', 'width=device-width, initial-scale=1');
  output.setXFrameOptionsMode(HtmlService.XFrameOptionsMode.ALLOWALL);
  output.setTitle('Upload File To Me');
  return output.setSandboxMode(HtmlService.SandboxMode.IFRAME);
}

function uploadFile(base64Data, fileName, folderId) {
	try {
		var splitBase = base64Data.split(','), type = splitBase[0].split(';')[0]
				.replace('data:', '');
		var byteCharacters = Utilities.base64Decode(splitBase[1]);
		var ss = Utilities.newBlob(byteCharacters, type);
		ss.setName(fileName);

		var folder = DriveApp.getFolderById(folderId);
		var files = folder.getFilesByName(fileName);
		var file;
		while (files.hasNext()) {
			// delete existing files with the same name.
			file = files.next();
			folder.removeFile(file);
		}
		file = folder.createFile(ss);
		return {
			'folderId' : folderId,
			'fileName' : file.getName()
		};
	} catch (e) {
		return {
			'error' : e.toString()
		};
	}
}

function createFolder(folderName) {
	try {
        var parentFolderId = "1wSMrbhopDQiOoaAWHV772cDuuLIxemjc";
		var parentFolder = DriveApp.getFolderById(parentFolderId);
		var folders = parentFolder.getFoldersByName(folderName);
		var folder;
		if (folders.hasNext()) {
			folder = folders.next();
		} else {
			folder = parentFolder.createFolder(folderName);
		}
		return {
			'folderId' : folder.getId()
		}
	} catch (e) {
		return {
			'error' : e.toString()
		}
	}
}

function hwUploadNotify(info){
  var token = "M0yiZNObYFoiLbhDR2taNTGc23Hx8WhotbcqtQ6DL2C";
  var message = "\n"+info.name + " 交作業啦！\n [Mail] " + info.mail 
  + "\n [Section] " + info.section 
  + "\n [Files] \n" + info.files
  + "\n [Feedback] \n" + info.feedback;
  sendLineNotify(message, token);
}

function testSendNotify() {
  var token = "M0yiZNObYFoiLbhDR2taNTGc23Hx8WhotbcqtQ6DL2C";
  var message = "testSendNotify";
  sendLineNotify(message, token);
}

function sendLineNotify(message, token){
  var options =
   {
     "method"  : "post",
     "payload" : {"message" : message},
     "headers" : {"Authorization" : "Bearer " + token}
   };
   UrlFetchApp.fetch("https://notify-api.line.me/api/notify", options);
}
